import React from "react"
import { Tag, Button } from "@blueprintjs/core"
import { __ } from "react-pe-utilities"
import ScalarField from "./ScalarField"

export default class Email extends ScalarField {
  isEnabled() {
    const { value } = this.state
    return (
      <div className={`d-flex w-100 align-items-center ${this.props.className}`}>
        <i className="fas fa-at " />
        <div className=" w-100 datetimer">
          <input
            type="text"
            className=""
            value={value || ""}
            onChange={this.onChange}
          />
        </div> 
      </div>
    )
  }

  isDesabled() {
    return (
      <div className={`datetimer ${this.props.className}`}>
        <i className="fas fa-at" />
        <div className="px-0 my-2">
          {
            this.props.value
              ? (
                <Tag minimal>
                  {`${this.props.value} `}
                </Tag>
              )
              : null
          }
        </div>
      </div>
    )
  }

  onChange = (evt) => {
    this.setState({ value: evt.currentTarget.value })
    this.on(evt.currentTarget.value)
  }

  on = (value) => {
    this.props.on(value, this.props.field, this.props.title)
  }
}
