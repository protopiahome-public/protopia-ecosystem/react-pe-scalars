import React from "react" 
import ScalarField from "./ScalarField"
import {MediaChooser} from "react-pe-useful" 
import { compose } from "recompose"
import { withApollo } from "react-apollo"
import { server_url__ } from "settings/config"
import { Button, Dialog } from '@blueprintjs/core'

//  Scalar  String

class Media extends ScalarField {

  isEnabled() {
    const { field } = this.props
    const { value } = this.state
    return <div className="my-2 w-100">
      <MediaChooser
        prefix={`_${field}${this.props.id}`}
        url={value}
        id=""
        ID=""
        padding={5}
        height={120}
        tab={this.tab}
        hideLib={this.props.hideLib}
        accept={this.props.accept}
        isUploadHide={this.props.isUploadHide}
        isURLHide={this.props.isURLHide}
        onChange={this.onMediaChange}
        server_url={server_url__()}
      />
    </div>
  }

  isDesabled() {
    const { value } = this.state
    const {width, height} = this.props
    return <div className="px-0 my-1 media-input container">
      <div
        style={{
          backgroundImage: `url(${value})`,
          backgroundSize: "cover",
          width: width ? width : 160,
          height: height ? height : 160,
          opacity: 0.8,
          margin: 6,
          cursor:"pointer"
        }}
        className="media-input"
        onClick={this.onOpen}
      />
      <Dialog
        isOpen={this.state.isOpen}
        onClose={this.onOpen}
      >
        <div className="flex-centered">
          <img src={value} alt="" style={{ maxHeight:"90vh" }} />
        </div>
      </Dialog>
    </div>
  }
  onOpen = () =>
  {
    this.setState({ isOpen: !this.state.isOpen })
  }
  onMediaChange = (value, file, id) => {
    // console.log(value);
    const state = { value }
    // state[this.props.field + "_name"] =

    this.on(value, this.props.field, file.name, id)
    this.setState(state)
  }

  on = (value, name, fileName, id=-1) => {
    const anoverField = {}
    anoverField[`${this.props.field}_id`] = id
    anoverField[`${this.props.field}_name`] = fileName

    // console.log(  value, this.props.field, this.props.title, anoverField );
    this.props.on(value, this.props.field, this.props.title, anoverField)
  }
}

export default  compose(
	withApollo, 
)(Media)
