import React, { Component, Fragment } from "react"
import { Button, Tag } from "@blueprintjs/core"
import { __ } from "react-pe-utilities"
import ScalarField from "./ScalarField"

export default class ImageCheckbox extends ScalarField 
{
	state = {
	    value: Array.isArray(this.props.value) ? this.props.value : [this.props.value],
	}

	componentWillReceiveProps(nextProps) {
	    if (nextProps.value) this.setState({ value: nextProps.value })
	}

	isEnabled() {
	  if (Array.isArray(this.props.values)) 
      {
	        return <div className="d-flex flex-wrap">
                {
                    this.props.values.map((e, i) => {
                        const checked = Array.isArray( this.state.value )
                            ?
                            this.state.value.filter(v => v === e._id).length > 0
                            :
                            false
                        return <Button
                            key={i}
                            className="scalar-imgage-radio-label"
                            onClick={evt => this.onClick(evt, i)}
                            v={e._id}
                            minimal={!checked}
                        >
                            <img
                                src={e.img}
                                className="scalar-imgage-radio-img"
                                style={{
                                    height: e.height,
                                    top: 0,
                                    opacity: checked
                                        ? 
                                        e.icon_opacity
                                            ? 
                                            e.icon_opacity
                                            : 
                                            1
                                        : 
                                        0.125,
                                }}
                            />
                            <div className="scalar-imgage-radio-title">
                                { e.title }
                            </div>
                            <div className="scalar-imgage-radio-description">
                                { e.desription }
                            </div>
                        </Button>
                    })
                }
            </div>
	  } 
	  // this.isDesabled()
	}

	isDesabled() 
    { 
	  return <div className="px-0 my-2">
        {
            this.props.value
                ? 
                <Tag minimal>
                    { `${this.props.value} `}
                </Tag>
                :					
                null
        }
        </div> 
	}

	onClick = (evt, i) => {
	  const value = evt.currentTarget.getAttribute( "v" )
      const state = { ...this.state }
      state.value[ i ] = state.value[ i ] ? false : value
	  this.setState( state )
	  this.on(  state.value )
	}

	on = (value) => {
	  // console.log(value, this.props.field, this.props.title);
	  if (this.props.on) this.props.on(value, this.props.field, this.props.title)
	}
}
