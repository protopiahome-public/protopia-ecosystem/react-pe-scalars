import React, { useEffect, useState } from "react"
import { __ } from "react-pe-utilities"
import {
  Button, Popover, Position,
} from "@blueprintjs/core"
import SubMenu from "./SubMenu"

//  Scalar  Boolean
const Boolean =  props =>
{
  const { title, description, commentary, sub_menus, field, prefix, editable, postfix } = props
  const [value, setValue] = useState( props.value === "fale" ? false : !!props.value )
  const col1 = title || description
    ? props.vertical
      ? "col-12 layout-label-vert "
      : "col-md-3  layout-label"
    : " hidden "
  const col2 = title || description
    ? props.vertical
      ? "col-12 layout-data-vert "
      : "col-md-9 layout-data"
    : " col-12 layout-data-vert "
  const descr = description
    ? (
      <Popover
        position={Position.RIGHT}
        interactionKind="hover"
        content={<div className="p-4 square" dangerouslySetInnerHTML={{ __html: __(description) }} />}
      >
        <Button minimal icon="help" />
      </Popover>
    )
    : null
  
  const isDesabled = () =>
  {
    return <div className="px-0 m-1">
    {
        value
          ?
          <i className="fas fa-chevron-down text-success" />
          :				
          <i className="fas fa-times text-danger" />
    }
    </div>
  }
  const isEnabled = () =>
  {
    const { field, _id, editable } = props 
    return (
      <div className="mt-2 mb-3">
        <input
          type="checkbox"
          className="checkbox"
          id={`bool_${field}${_id}`}
          onChange={ handleCheckClick }
          value={value}
          checked={value === true} 
        />
        <label htmlFor={`bool_${field}${ _id }`}>
          { __(value ? "Yes" : "No") }
        </label>
      </div>
    )
  }
  const handleCheckClick = evt =>
  {
    console.log(value)
    const val = !value;
    setValue(val);
    //props.on(val);
    if (props.on) 
    {
      const value = props.prefix ? val.replace(props.prefix, "") : val
      props.on(value, props.field, props.title)
    }
  }
  const onSubMenu = (val, field, title, prefix) =>
  {
    if (!props.onSubMenu) return 
    props.onSubMenu(val, field)
  }

  return  <div className="row dat strob01 " row_data={ field }>
    <div className={col1}>
      {__(title)}
      {descr}
    </div>
    <div className={col2}>
      <div className="d-flex flex-grow-100">
        {
          prefix ? <span className="prefix">{prefix}</span> : null
        }
        {
          editable ? isEnabled() : isDesabled()
        }
        {
          postfix  ? <span className="postfix">{postfix}</span> : null
        }
      </div>
      <SubMenu
        sub_menus={sub_menus}
        on={onSubMenu}
      />
    </div>
    {
      commentary
        ?
        <div className="scalar-commentary px-3 col-md-9 offset-md-3" dangerouslySetInnerHTML={{ __html: __(commentary) }} />
        :
        null
    }

  </div>
}
export default Boolean

// export default class Boolean extends ScalarField {
//   constructor(props) {
//     super(props)
//     this.state = {
//       value: this.props.value !== "false" ? !!this.props.value : false
//     }
//   }

//   isEnabled() {
//     const { field } = this.props
//     const { value } = this.state
//     return (
//       <div className="mt-2 mb-3">
//         <input
//           type="checkbox"
//           className="checkbox"
//           id={`bool_${field}${this.props._id}`}
//           onChange={this.handleCheckClick}
//           value={value}
//           checked={value === true}
//           disabled={!this.props.editable}
//         />
//         <label htmlFor={`bool_${field}${this.props._id}`}>
//           { __(value ? "Yes" : "No") }
//         </label>
//       </div>
//     )
//   }

//   isDesabled() 
//   {
//     return (
//       <div className="px-0 m-1">
//         {
// 			this.props.value
// 			  ? <i className="fas fa-chevron-down text-success" />
// 			  :				<i className="fas fa-times text-danger" />
// 		}
//       </div>
//     )
//   }

// 	handleCheckClick(evt)
//   { 
//     console.log(this.state, this.props.field);
//     console.log(this.state.value, this.props.field);
// 	  const value = !this.state.value	  
// 	  this.setState({ value })
// 	  this.on(value)
// 	}
// }
