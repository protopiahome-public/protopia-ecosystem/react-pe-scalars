import React from "react"
import { Button } from "@blueprintjs/core" 
import ScalarField from "./ScalarField"
import {CodeEditor} from "react-pe-useful"

export default class HTMLEditor extends ScalarField 
{
  isEnabled() 
  {
    const { value } = this.props
    const val = value ? value : "" 
    return (
      <div className={`d-flex w-100 align-items-start ${this.props.className ? this.props.className : ""}`}> 
        <div className=" pe-code-editor ">
          <CodeEditor
                value={val}
                onChange={this.onChange}
          />
        </div>
      </div>
    )
  }
  onChange = code =>
  {
    this.setState({ value: code })
    this.on(code)
  }

}
 