import React from "react"
import { Button, Tag } from "@blueprintjs/core"
import { __ } from "react-pe-utilities"
import ScalarField from "./ScalarField"

// TODO extends ScalarField
export default class URL extends ScalarField {

	isEnabled() {
		const { value } = this.state
		return <div className="w-100 d-flex align-items-center">
			<i className="fas fa-desktop" />
			<input
				type="text"
				className="form-control input dark"
				value={value || ""}
				onChange={this.onChange}
			/>
		</div>
	}
	isDesabled() {
		return <div className="px-0 my-2 max-width-100">
		{
			this.props.value
				? (
					<a href={this.props.value} target="_blank" className="bp3-text-overflow-ellipsis">
						{`${this.props.value} `}
					</a>
				)
				: null
		}
		</div>
	}
}
