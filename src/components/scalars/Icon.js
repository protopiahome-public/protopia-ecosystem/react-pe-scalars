import React, {Suspense, lazy} from "react"
import { Tag, Button, Intent, Tabs, Tab } from "@blueprintjs/core"
import { __ } from "react-pe-utilities"
import ScalarField from "./ScalarField"
import {Loading, FontAwesome} from "react-pe-useful"
import {MediaChooser} from "react-pe-useful"  
// const FontAwesome = lazy(
// 	() => import("react-pe-useful")
// 		.then(module => ({ default: module.FontAwesome }))
// )

export default class Icon extends ScalarField {
  constructor(props)
  {
    super(props);
    if(!this.state) this.state = {}
    this.state = {...this.state, ...this.extractValue(props.value)}
    this.state.navBarID = this.state.isSVG ? "image" : "icon"
  }
  
  componentWillReceiveProps(nextProps) {
    if ( this.state.value !== nextProps.value) 
    {
      const values = this.extractValue( nextProps.value )
      //console.log( values )
      this.setState({ value: values.value, size: values.size, navBarID : values.isSVG ? "image" : "icon" })

    }
  }
  extractValue(val)
  {
    if(typeof val !== "string") return { value:"", size:"", isSVG: true }
    let isSVG = val ? val.split(".").length > 1 : false, 
          value = val, 
          size = ""
    if(!isSVG)
    {
      value = val.replaceAll("fa-1x", "").replaceAll("fa-2x", "").replaceAll("fa-3x", "")
      size = val.replaceAll( value, "" )
      size = size.indexOf("fa-1x") ? " fa-1x" : 
        size.indexOf("fa-2x") ? " fa-2x" : 
          size.indexOf("fa-3x") ? " fa-3x" : 
          size
    }
    return {value, size, isSVG}
  }
  onNavBar = navBarID => this.setState({ navBarID })

  isEnabled() {
    const {isSVG, value} = this.state
    //return isSVG ? this.isIcon(): this.isImage()
    return <div style={{height:220, width:"100%"}}>
      <Tabs
        animate={false}
        selectedTabId={this.state.navBarID}
        onChange={this.onNavBar}
        vertical
        className="w-100 icon-scalar-container"
      >
        <Tab
          id="image"
          title={<div className="d-flex flex-column align-items-center">
            <i className="far fa-image "/>
            <div className="super-small">
              {__("image")}
            </div>
          </div>}
          className="w-100"
          panel={
            <MediaChooser
              url={value}
              onChange={this.onImage}
              id=""
              isUploadHide={this.props.isUploadHide}
              isURLHide={this.props.isURLHide}
            />
          }
        />
        <Tab
          id="icon"
          title={<div className="d-flex flex-column align-items-center">
            <i className="fas fa-asterisk "/>
            <div className="super-small">
              {__("icon")}
            </div>
          </div>}
          className="w-100"
          panel={ this.getIcon() }
        />
      </Tabs>      
    </div>
  }
  getIcon() {
    const { value, size } = this.state
    return (
      <div className={`d-flex w-100 ${this.props.className}`} style={{height:199 }}>
        {/* <div className="px-3"> {this.props.value} </div>
        <div className="px-3"> {value} </div>
        <div className="px-3"> {size} </div> */}
        <Suspense fallback={<Loading />}>
          <FontAwesome value={value} onChange={this.onValue} />
        </Suspense>
        <Button 
          minimal 
          intent={size == "fa-1x" ? Intent.DANGER : Intent.NONE} 
          large={true} 
          className="ml-auto font-weight-bold " 
          onClick={() => this.onSize(1)}
        >
          1x
        </Button>
        <Button 
          minimal 
          intent={size == "fa-2x" ? Intent.DANGER : Intent.NONE} 
          large={true} 
          className=" font-weight-bold " 
          onClick={() => this.onSize(2)}
        >
          2x
        </Button>
        <Button 
          minimal 
          intent={size == "fa-3x" ? Intent.DANGER : Intent.NONE} 
          large={true} 
          className=" font-weight-bold " 
          onClick={() => this.onSize(3)}
        >
          3x
        </Button> 
      </div>
    )
  }

  isDesabled() {
    return (
      <div className={` ${this.props.className}`}>
        <i className="fas fa-at" />
        <div className="px-0 my-2">
          {
            this.props.value
              ? (
                <Tag minimal>
                  {`${this.props.value} `}
                </Tag>
              )
              : null
          }
        </div>
      </div>
    )
  }
  onValue = value =>
  {
    this.setState({ value })
    this.onChangeIcon()
  }
  onSize = size => {
    this.setState({ size: " fa-" + size +"x" })
    this.onChangeIcon()
  }
  onChangeIcon = () => {
    //console.log(this.state.value, this.state.size)
    setTimeout(() => this.on(this.state.value + this.state.size), 200)    
  }
  onImage = (src) =>
  {
    setTimeout(() => this.on(src), 200)  
  }
}
