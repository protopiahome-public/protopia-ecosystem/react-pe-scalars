import React from "react" 
import ScalarField from "./ScalarField"
//import TextEditor from "../../utilities/TextEditor"
import {TextEditor} from "react-pe-useful" 


export default class Text extends ScalarField {
	isEnabled() {
		return this.textEditor()
	}

	isDesabled() {
		return this.props.value
	}

	onChange = (data) => {
	  this.setState({ value: data })
	  this.on(data)
	}

	textEditor() {
	  return <TextEditor onChange={this.onChange} text={this.state.value} />
	  /* return <textarea onChange={this.onChange} className="form-control" rows="10" value={this.state.value} >
			{this.state.value}
		</textarea>;
		*/
	}
}
