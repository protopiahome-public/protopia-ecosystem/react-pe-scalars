import React, { Component, Fragment } from "react"
import { Button, Tag } from "@blueprintjs/core"
import { __ } from "react-pe-utilities"
import ScalarField from "./ScalarField"

export default class ImageRadio extends ScalarField 
{ 
	componentWillReceiveProps(nextProps) {
	  if (nextProps.value) this.setState({ value: nextProps.value })
	}

	isEnabled() {
	  if (Array.isArray(this.props.values)) 
	  {
		const curVal = this.props.values.filter( e => e._id === this.state.value)[0]
	    return <div>
			<div className="d-flex flex-wrap">
			{
				this.props.values.map((e, i) => {
					if(e.hidden) return null
					return <Button
						key={i}
						className="scalar-imgage-radio-label"
						onClick={this.onClick}
						v={e._id}
						minimal={e._id != this.state.value}
					>
						<img
						src={e.img}
						className="scalar-imgage-radio-img"
						style={{
							height: e.height,
							top: 0,
							opacity: e._id == this.state.value
							? 
							e.icon_opacity
								? 
								e.icon_opacity
								: 
								1
							: 
							0.125,
						}}
						/>
						<div className="scalar-imgage-radio-title">
							{ e.title }
						</div>
						<div className="scalar-imgage-radio-description">
							{ e.desription }
						</div>
					</Button>
	  			})
			}
			</div>
			<div 
				className="scalar-commentary" 
				dangerouslySetInnerHTML={{ __html: __(curVal ? curVal.commentary : null) }} 
			/>
		</div>
	}

	  this.isDesabled()
	}

	isDesabled() {
	  const { field, title } = this.props
	  const { value } = this.state
	  return <div className="px-0 my-2">
		{
			this.props.value
				? 
				<Tag minimal>
					{ `${this.props.value} `}
				</Tag>
				:					
				null
		}
		</div> 
	}

	onClick = (evt) => {
	  const value = evt.currentTarget.getAttribute("v")
	  this.setState({ value })
	  this.on(value)
	}

	on = (value) => {
	  // console.log(value, this.props.field, this.props.title);
	  if (this.props.on) this.props.on(value, this.props.field, this.props.title)
	}
}
