import { Button,  Popover } from "@blueprintjs/core"
import React from "react" 
import FieldInput from ".." 

const SubMenu = ({ sub_menus, m, on }) => {
    if (!(Array.isArray(sub_menus) && sub_menus.length > 0))
        return null
    
    //console.log(sub_menus)
    const fields = sub_menus.map((menu, i) => {

        // есть ли у этого поля разрешаемое (demand)?
        // Если есть, то его значение соответствует 
        // хотя бы одному из разрешаемых?
        const notDemand = menu.demand
            && menu.demand.value.filter(e => {
                const df = sub_menus.filter(fi => fi.field == menu.demand.field)[0]
                //console.log( sub_menus )
                // console.log( menu.demand.field )
                // console.log( df.field )
                // console.log( e )
                // console.log( menu.origin.data[ df.field ] )
                /*
                *	menu.value - значения полей данного инстанса
                *	menu.demand - название разрешаемого поля
                */
                return df ? menu.origin.data[ df.field ] == e : false
            }).length == 0;
        

        if (!notDemand) {
            //console.log(menu.value, menu.field);
            return <FieldInput
                field={menu.field}
                key={i}
                on={(value, dopol) => on(value, menu.field, dopol)}
                onChange={(value, dopol) => on(value, menu.field, dopol)}
                visibled_value={menu.visibled_value}
                {...menu}
                origin={menu.origin}
                sourceData={menu ? menu : ""}
                editable
                value={menu.value}
                vertical={true}

            />
        }
    })
    return <div className="position-absolute right bottom flex-grow-1">
        <Popover
            content={
                <div style={{ width: 400, padding: 10, maxHeight:"80vh" }} className=" overflow-y-auto ">
                    {fields}
                </div>
            }
        >
            <Button minimal icon={<i className="fas fa-ellipsis-h" />} />
        </Popover>
    </div>
}
export default SubMenu