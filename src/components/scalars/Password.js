import React from "react"
import { Tag, Button, } from "@blueprintjs/core"
import { __ } from "react-pe-utilities"
import ScalarField from "./ScalarField"

//  Scalar  String

export default class Password extends ScalarField {
  isEnabled() {
    const { field, title } = this.props
    const { value } = this.state
    return (
      <div className="d-flex w-100">
        <input
          autoFocus={this.props.autoFocus}
          type="password"
          className={ (this.props.className ? this.props.className : "form-control input dark") + " w-100 " }
          value={value || ""}
          onChange={this.onChange} 
        /> 
      </div>
    )
  }

  isDesabled() {
    return (
      <div className="px-0 mb-1">
        <Tag minimal className="m-1">
          {" ****** "}
        </Tag>
      </div>
    )
  }
}
