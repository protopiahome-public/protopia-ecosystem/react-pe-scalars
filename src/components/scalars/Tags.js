import React, { Component } from "react" 
import { __ } from "react-pe-utilities"
import ScalarField from "./ScalarField"
import TagScalars from "./tags/TagScalars"

//  Scalar  Int

export default class Tags extends ScalarField {
    isEnabled() {
        const { field, title } = this.props
        const { value } = this.state
        return <>
            <TagScalars
                { ...this.props }
                value={ value }
                values={ this.props.values }
                onChange={ this.props.onChange }
            />
        </>
    }
}