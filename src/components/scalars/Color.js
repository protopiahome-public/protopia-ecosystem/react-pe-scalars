import React, { Component } from "react"
import {
	Tag, ButtonGroup, Button, Intent,
} from "@blueprintjs/core"
import rgbHex from "rgb-hex"
import { __ } from "react-pe-utilities"
import {ColorPicker} from "react-pe-useful"
import ScalarField from "./ScalarField"

export default class Color extends ScalarField {
	constructor(props) {
		super(props)
		let current = "plainColor"
		if (props.value && this.getGradientData().length > 1) {
			current = "gradient"
		}
		this.state = {
			value: props.value,
			mode: props.mode ? props.mode : "color", //gradient, all
			current,
			gradientDestination: this.getGradientDestination()
		}
		//console.log(current)
	}
	distinations = () => {
		return [
			"up",
			"right",
			"down",
			"left",
		]
	}

	isDesabled() {
		const {
			field, title, value, extended_link, external_link_data,
		} = this.props
		return (
			<div className={`datetimer ${this.props.className}`}>
				<div style={{ width: 100, height: 25, backgroundColor: this.state.value }} />
			</div>
		)
	}

	isEnabled() {
		return (
			<div className={`color w-100 d-flex ${this.props.className}`}>
				{this[this.state.current]()}
				{this.switchers()} 
			</div>
		)
	}
	plainColor = () => {
		const { value } = this.state
		// console.log( value )
		return <>
			<ColorPicker color={this.getSingleColor()} onChoose={this.onColor} />
			<input
				type="string"
				className="form-control input dark"
				value={ value  === null ? "" : value }
				onChange={this.onChange}
			/>
		</>
	}
	gradient = () => {
		const colors = this.getGradientData()
		return <div className="w-100 d-flex">
			<ColorPicker color={colors[0]} onChoose={color => this.onColorGradient(color, 0)} />
			<ColorPicker color={colors[1]} onChoose={color => this.onColorGradient(color, 1)} />
			<Button
				minimal
				icon={"arrow-" + this.state.gradientDestination}
				style={{ minWidth: 45 }}
				onClick={this.onDestination}
			/>
			<input disabled className="form-control input dark" />
		</div>
	}
	onClear = (evt) => {
	  this.setState({ value: null, gradientDestination:"up", current:"plainColor" })
	  this.on(null)
	}
	onColor = (color) => {
		const c = color.rgb.a == 1.0
			? color.hex
			: color.rgb.a == 0.0
				? "transparent"
				: `#${rgbHex(color.rgb.r, color.rgb.g, color.rgb.b, color.rgb.a)}`
		this.setState({ value: c })
		this.on(c)
	}
	onColorGradient = (color, num) => {
		const c = color.rgb.a == 1.0
			? color.hex
			: color.rgb.a == 0.0
				? "transparent"
				: `#${rgbHex(color.rgb.r, color.rgb.g, color.rgb.b, color.rgb.a)}`
		const colors = this.getGradientData()
		colors[num] = c;
		colors.push(this.state.gradientDestination)
		const newColors = colors.join("==color==")
		this.setState({ color: newColors })
		this.on(newColors)
	}
	onDestination = () => {
		let dest = this.state.gradientDestination
		this.distinations().forEach((e, i) => {
			if (e == this.state.gradientDestination) {
				dest = this.distinations()[(i + 1) % this.distinations().length]
			}
		})
		let colors = this.getGradientData()
		if (colors.length > 1) {
			colors[colors.length == 2 ? colors.length : colors.length - 1] = dest
			this.on(colors.join("==color=="))
		}
		this.setState({ gradientDestination: dest })
	}
	getGradientData = () => {
		let colors = this.state.value ? this.state.value.split("==color==") : []
		colors.pop();
		return colors
	}
	getGradientDestination = () => {
		let colors = this.state.value ? this.state.value.split("==color==") : []
		let type = colors.pop();
		return colors.length > 1 ? type : "up"
	}
	getSingleColor() {
		let colors = this.state.value ? this.state.value.split("==color==") : []
		//console.log(colors.length)
		if (colors.length < 2)
			return this.state.value
	}

	switchers = () => {
		return this.state.mode == "all"
			?
			<ButtonGroup minimal>
				<Button
					icon="stop"
					intent={this.state.current == "plainColor" ? Intent.DANGER : Intent.NONE}
					className="hint hint--top d-flex"
					data-hint={__("plain color")}
					onClick={() => this.setState({ current: "plainColor" })}
				/>
				<Button
					icon="list"
					intent={this.state.current == "gradient" ? Intent.DANGER : Intent.NONE}
					className="hint hint--top d-flex"
					data-hint={__("gradient")}
					onClick={() => this.setState({ current: "gradient" })}
				/>
			</ButtonGroup>
			:
			null
	}

	onChange = (evt) => {
		this.setState({ value: evt.currentTarget.value })
		this.on(evt.currentTarget.value)
	}

	on = (value) => {
		this.props.on(value, this.props.field, this.props.title)
	}
}
