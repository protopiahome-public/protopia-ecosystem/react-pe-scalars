import React, { Component, Suspense } from "react"
import { Button } from "@blueprintjs/core"
import PhoneInput, { isValidPhoneNumber } from "react-phone-number-input"
import "react-responsive-ui/style.css"
import { __ } from "react-pe-utilities"
import { Loading } from "react-pe-useful" 

// TODO extends ScalarField
export default class Phone extends Component {
	state = { value: this.props.value }

	render() {
		const { field, title } = this.props
		const { value } = this.state
		const col1 = this.props.vertical ? "col-12 layout-label-vert" : "col-md-3  layout-label"
		const col2 = this.props.vertical ? "col-12 layout-data-vert" : "col-md-9 layout-data"
		return (
			<div className="row dat" key={field}>
				<div className={col1}>
					{__(title)}
				</div>
				<div className={col2 + " align-items-start"}>
					{
						this.props.editable
							? (
								<div className="mb-4 w-100">
									<Suspense fallback={<Loading />}>
										<PhoneInput
											country="RU"
											placeholder={__("Enter phone number")}
											value={this.state.value}
											onChange={this.onChange}
											error={value ? (isValidPhoneNumber(value) ? undefined : __("Invalid phone number")) : __("")}
										/>
									</Suspense>
								</div>
							)
							: <div className="px-0 my-2">{this.props.value}</div>
					} 
				</div>
			</div>
		)
	}

	onChange = (value) => {
		this.setState({ value })
		this.on(value)
	}

	on = (value) => {
		this.props.on(value, this.props.field, this.props.title)
	}
}
