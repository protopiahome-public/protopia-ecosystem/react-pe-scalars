import React from "react"
import { getFields } from "react-pe-layouts"
import { isCapability } from "../user"
import FieldInput from "./index"

// https://jsdoc.app/tags-deprecated.html

/**
 * @deprecated since version 1.0
 */
export default function drawForms(props) {
  if (props.isMain) {
    // console.log( this.props.addList )
    // console.log( this.state.activate_sources )
    // console.log( this.props.data.themes )
  }
  const fields = []
  const _fields = getFields(props.data_type)
  for (const field in _fields) {
    // if(this.props.isMain)
    //	console.log( this.state[field] );

    if (
      field == "id"
            || field == "admin_data"
            || (
              isCapability(_fields[field].caps, "")
            )
    ) continue
    const editable = typeof props.editable !== "undefined" ? this.props.editable : _fields[field].editable
    // добавляем данные для формирования "умной" ссылки, если в схеме указано, что она есть
    const external_link_data = { orig: { id: props.id } }
    if (_fields[field].external_state) {
      // добавляем в ссылку данные дочерних элементов объекта, указанные в схеме
      for (const es in _fields[field].external_state) {
        external_link_data[es] = {
          component: _fields[es].component,
          ...props[es],
        }
      }
    }
    const compp = _fields[field].component
  }

  return fields
}
