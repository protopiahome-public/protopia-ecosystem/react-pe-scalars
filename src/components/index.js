import React, { Component } from "react"
import SubMenu from "./scalars/SubMenu"
import addEmpty from "./scalars/addEmpty"
import String from "./scalars/String"
import Password from "./scalars/Password"
import Address from "./scalars/Address"
import TextField from "./scalars/TextField"
import ID from "./scalars/ID"
import URL from "./scalars/URL"
import Email from "./scalars/Email"
import Link from "./scalars/Link"
import Int from "./scalars/Int"
import ExtendedLink from "./scalars/ExtendedLink"
import DateTime from "./scalars/DateTime"
import RGB from "./scalars/RGB"
import Color from "./scalars/Color"
import Geo from "./scalars/Geo"
import MultiSelect from "./scalars/MultiSelect"
//import Selector from "./scalars/Selector"
import Text from "./scalars/Text"
import Radio from "./scalars/Radio"
import Checkbox from "./scalars/Checkbox"
import Array from "./scalars/Array"
import Phone from "./scalars/Phone"
import Boolean from "./scalars/Boolean"
import Reloadbled from "./scalars/Reloadbled"
import Media from "./scalars/Media"
import Upload from "./scalars/Upload"
import FloatSlider from "./scalars/FloatSlider"
import ImageRadio from "./scalars/ImageRadio"
import ImageCheckbox from "./scalars/ImageCheckbox"
import HTMLEditor from "./scalars/HTMLEditor"
import DataTypeSelector from "./scalars/DataTypeSelector"
import Icon from "./scalars/Icon"
import ScalarField from "./scalars/ScalarField"

import ArrayPlus from "./components/ArrayPlus"
import ExternalPlus from "./components/ExternalPlus"
import ComponentPlus from "./components/ComponentPlus"
import { initArea } from "react-pe-utilities"
import "./style.css"
import Tags from "./scalars/Tags"

export { ScalarField }
export { SubMenu }
export { addEmpty }
export { Color }
export { HTMLEditor }
export { String }
export { Password }
export { Address }
export { TextField }
export { Email }
export { URL }
export { ID }
export { Link }
export { ExtendedLink }
export { DateTime }
export { RGB }
export { Geo }
export { MultiSelect }
export { Radio }
export { Checkbox }
export { Phone }
export { Array }
export { Boolean }
export { Reloadbled }
export { Media }
export { Int }
export { Text }
export { FloatSlider }
export { ImageRadio }
export { ImageCheckbox }
export { Icon }

// TODO проверить все ли перенесено в отдельные файлы
export function toFieldInput(params) {
  //console.log(params);
  const {
    field,
    description,
    commentary,
    external_state,
    external_link_data,
    vertical,
    list,
    addList,
    isOpen,
    title,
    value,
    onChange,
    type,
    step_size,
    label_step_size,
    min,
    max,
    sub_menus,
    not_clear
  } = params

  const editable = typeof params.editable !== "undefined" ? params.editable : true
  const visibled_value = params.visibled_value
    ? params.visibled_value
    : "title"

  if (params.kind === "type" && params.component) {

  } else if (params.type === "array" && params.component === "string") {

  } else {
    params.component = params.type
  }
  if (params.visualization) {
    //console.log(params)
    return initArea(params.visualization, { ...params })
  }
  switch (type) {
    case "data_type":
      return (
        <DataTypeSelector
          title={title}
          description={description}
          commentary={commentary}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          not_clear={not_clear}
        />
      ) 
    case "html":
      return (
        <HTMLEditor
          title={title}
          description={description}
          commentary={commentary}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          value={value}
          default={params.default}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      )
      // return (
      //   <TextField
      //     title={title}
      //     description={description}
      //     commentary={commentary}
      //     field={field}
      //     prefix={params.prefix}
      //     posrfix={params.posrfix}
      //     editable={editable}
      //     className={params.className}
      //     visibled_value={visibled_value}
      //     style={params.style}
      //     value={value}
      //     default={params.default}
      //     values={params.values}
      //     vertical={vertical}
      //     sub_menus={sub_menus}
      //     on={onChange}
      //     onSubMenu={params.onSubMenu}
      //     not_clear={not_clear}
      //   />
      // )
    case "array":
      return ArrayPlus(params) 
    case "external":
      return ExternalPlus(params) 
    case "component":
      return ComponentPlus(params) 
    case "text":
      return (
        <Text
          title={title}
          description={description}
          commentary={commentary}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          value={value}
          default={params.default}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "int":
    case "number":
      return (
        <Int
          title={title}
          description={description}
          commentary={commentary}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          step_size={params.step_size}
          style={params.style}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "slider":
      return (
        <FloatSlider
          title={title}
          description={description}
          commentary={commentary}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          default={params.default}
          value={value || 0}
          values={params.values}
          vertical={vertical}
          on={onChange}
          onSubMenu={params.onSubMenu}
          min={min}
          max={max}
          step_size={step_size}
          label_step_size={label_step_size}
          sub_menus={sub_menus}
          not_clear={not_clear}
        />
      ) 
    case "media":
      // TODO : требует обязательное поле с ID - имя поля + постфикс "_id"
      return (
        <Media
          title={title}
          description={description}
          commentary={commentary}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          id={params._id}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          default={params.default}
          hideLib={params.hideLib}
          tab={params.tab}
          accept={params.accept}
          isUploadHide={params.isUploadHide}
          isURLHide={params.isURLHide}
          value={value}
          values={params.values}
          vertical={vertical}
          on={onChange}
          onSubMenu={params.onSubMenu}
          media_id={params[`${field}_id`]}
          sub_menus={sub_menus}
          not_clear={not_clear}
          width={params.width}
          height={params.height}
        />
      ) 
    case "upload":
      // TODO : требует обязательное поле с ID - имя поля + постфикс "_id"
      return (
        <Upload
          title={title}
          description={description}
          commentary={commentary}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          id={params._id}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "date":
      return (
        <DateTime
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          default={params.default}
          value={value}
          values={params.values}
          visibled_value={visibled_value}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "color":
      return (
        <Color
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          default={params.default}
          value={value}
          visibled_value={visibled_value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          mode={params.mode} 
          not_clear={not_clear}
        />
      ) 
    case "rgb":
      return (
        <RGB
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          default={params.default}
          value={value}
          visibled_value={visibled_value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "boolean":
      return (
        <Boolean
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          _id={params._id}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "phone":
      return (
        <Phone
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "geo":
      // console.log(params._id);
      return (
        <Geo
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          _id={params._id}
          on={onChange}
          onSubMenu={params.onSubMenu}
          isOpen={isOpen}
          not_clear={not_clear}
        />
      ) 
    case "address":
      // console.log(params._id);
      return (
        <Address
          component={params.component}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          field={field}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "select":
      const listData = []
      if (addList) {
        // listData = data[aq].concat( addList );
      } else if (list) {
        // listData = list;
      } else {
        // listData = data[aq];
      }
      // console.log( params )
      return (
        <MultiSelect
          component={params.component}
          placeholder={params.placeholder}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          default={params.default}
          value={value}
          multiple={params.multiple}
          values={params.values}
          vertical={vertical}
          on={onChange}
          onSubMenu={params.onSubMenu}
          data={listData}
          sub_menus={sub_menus}
          not_clear={not_clear}
        />
      ) 
    case "checkbox":
      return (
        <Checkbox
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "image_checkbox":
      return (
        <ImageCheckbox
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "radio":
      // console.log(params);
      return <>
        <Radio
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      </> 
    case "tags":
      return <>
        <Tags
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      </> 
    case "link":
      // console.log(value);
      return (
        <Link
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          className={params.className}
          default={params.default}
          value={value}
          _id={params._id}
          external_state={external_state}
          external_link_data={external_link_data}
          vertical={vertical}
          sub_menus={sub_menus}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "email":
      return (
        <Email
          title={title}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          style={params.style}
          default={params.default}
          value={value}
          visibled_value={visibled_value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "url":
      return ( 
        <URL
          title={title}
          field={field}
          prefix={params.prefix}
          postfix={params.postfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "reloaditabled":
      return (
        <Reloadbled
          title={title}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "id":
      return (
        <ID
          title={title}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          style={params.style}
          visibled_value={visibled_value}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      ) 
    case "password":
      return (
        <Password
          title={title}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          style={params.style}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      )
    case "image_radio":
      return (
        <ImageRadio
          component={params.component}
          field={field}
          title={title}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          default={params.default}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          _id={params._id}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      )
    case "icon":
      return <Icon
        title={title}
        field={field}
        prefix={params.prefix}
        posrfix={params.posrfix}
        description={description}
        commentary={commentary}
        editable={editable}
        className={params.className}
        visibled_value={visibled_value}
        default={params.default}
        isUploadHide={params.isUploadHide}
        isURLHide={params.isURLHide}
        style={params.style}
        value={value}
        values={params.values}
        vertical={vertical}
        sub_menus={sub_menus}
        on={onChange}
        onSubMenu={params.onSubMenu}
        not_clear={not_clear}
      />
    case "string":
    default:
      return (
        <String
          title={title}
          field={field}
          prefix={params.prefix}
          posrfix={params.posrfix}
          description={description}
          commentary={commentary}
          editable={editable}
          className={params.className}
          visibled_value={visibled_value}
          default={params.default}
          style={params.style}
          value={value}
          values={params.values}
          vertical={vertical}
          sub_menus={sub_menus}
          on={onChange}
          onSubMenu={params.onSubMenu}
          not_clear={not_clear}
        />
      )
  }
}

export default class FieldInput extends Component {
  constructor(props) {
    super(props)
    const state = {}
    for (const n in props) {
      state[n] = props[n]
    }
    this.state = state
  }

  componentWillReceiveProps(nextProps) {
    // console.log({...nextProps})
    // this.setState({...nextProps})
    const state = {}
    for (const n in nextProps) {
      if (
        nextProps[n] !== this.state[n] && 
        typeof nextProps[n] !== "function" && 
        typeof nextProps[n] !== "undefined" && 
        !isNaN(nextProps[n]) &&
        n !== "sub_menus"
      ) {
        state[n] = nextProps[n]
        // console.log( state[n], nextProps[n] );
      }
    }

    //console.log( this.state );
    //console.log( Object.keys(state) );
    if (Object.keys(state).length > 0) {
      this.setState(state)
    }
  }

  render() {
    // console.log(this.state);
    return toFieldInput({ ...this.props })
  }
}
