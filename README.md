# ProtopiaEcosystem scalar input forms

Библиотека для  [ProtopiaEcosystem React Client](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/)

Данный пакет автоматически устанавливается в **Create-react-app приложение** и предоставляет разработчикам сторонних модулей необходимый набор Компонентов (визуальных утилит) для ввода редактируемых параметров.

!Deprecated