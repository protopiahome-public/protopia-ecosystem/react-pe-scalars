function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Tag, Popover, Classes, Button, ButtonGroup, Intent } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
export default class String extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      isOpen: false
    });

    _defineProperty(this, "onRemove", () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
      this.props.onRemove(this.props._id);
    });

    _defineProperty(this, "onClick", () => {//
    });

    _defineProperty(this, "onToggle", () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    });
  }

  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Popover, {
      popoverClassName: Classes.POPOVER_CONTENT_SIZING,
      portalClassName: "foo",
      isOpen: this.state.isOpen,
      content: /*#__PURE__*/React.createElement("div", {
        className: "p-2"
      }, /*#__PURE__*/React.createElement("div", {
        className: "font-weight-light text-center"
      }, __("Delete this?")), /*#__PURE__*/React.createElement(ButtonGroup, null, /*#__PURE__*/React.createElement(Button, {
        fill: true,
        onClick: this.onRemove,
        intent: Intent.DANGER,
        minimal: true
      }, __("Yes")), /*#__PURE__*/React.createElement(Button, {
        fill: true,
        onClick: this.onToggle,
        intent: Intent.SUCCESS,
        minimal: true
      }, __("No"))))
    }, /*#__PURE__*/React.createElement(Tag, {
      minimal: true,
      large: false,
      onRemove: this.onToggle,
      onClick: this.onClick,
      className: "m-1",
      intent: Intent.PRIMARY
    }, this.props.value)));
  }

}