function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Tag, ButtonGroup, Button, Intent, NumericInput } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField"; //  Scalar  Int

export default class Int extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onChange", _valueAsNumber => {
      this.setState({
        value: _valueAsNumber
      });
      this.on(_valueAsNumber);
    });
  }

  isEnabled() {
    const {
      field,
      title
    } = this.props;
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(NumericInput, {
      large: true,
      autoFocus: this.props.autoFocus,
      className: this.props.className ? this.props.className : "form-control input dark",
      value: value || "",
      stepSize: this.props.step_size,
      onValueChange: this.onChange
    }));
  }

}