function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Suspense } from "react";
import { Intent, Tag, Button, ButtonGroup, Position, Popover, InputGroup } from "@blueprintjs/core";
import chroma from "chroma-js";
import { __ } from "react-pe-utilities";
import { Loading } from "react-pe-useful";
import ScalarField from "./ScalarField";
import Select from "react-select"; // https://github.com/JedWatson/react-select

import { getIdName } from "react-pe-layouts";
export default class MultiSelect extends ScalarField {
  constructor(props) {
    super(props);

    _defineProperty(this, "getElementByValue", element => {
      const visible_value = this.props.visibled_value ? this.props.visibled_value : "title";

      if (typeof element === "array") {
        return element.map(e => this.getElementByValue(e));
      } else if (element._id) {
        return {
          value: element._id,
          label: element.title,
          color: "#000"
        };
      } else if (element[getIdName()]) {
        return {
          value: element[getIdName()],
          label: element[visible_value],
          color: "#000"
        };
      } else typeof element === "string" || typeof element === "number";

      {
        return {
          value: element,
          label: element,
          color: "#000"
        };
      }
    });

    _defineProperty(this, "handleChange", (newValue, actionMeta) => {
      if (this.props.multiple) {
        const sel = newValue || [];
        const selected = sel.map(e => {
          const value = {};
          value[getIdName()] = e.value;
          const label = Array.isArray(this.props.visibled_value) ? this.props.visibled_value.map(ee => e[ee]).join(" ") : e[this.props.visibled_value];
          value[this.props.visibled_value] = e.label;
          return value;
        });
        this.setState({
          value: selected
        });
        this.on(selected);
      } else {
        console.log(newValue);
        this.setState({
          value: newValue
        });
        this.on(newValue.value);
      }
    });

    _defineProperty(this, "onChange", evt => {
      const selected = evt.currentTarget.selectedOptions;
      const value = [];

      for (const i in selected) {
        if (!selected[i].value) continue;
        const v = {
          _id: selected[i].value
        };
        v[this.props.visibled_value] = selected[i].label;
        value.push(v);
      }

      console.log(value); // this.setState({value: [selected]});
      // this.on( [selected] );
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });

    const default_value = {};
    default_value[getIdName()] = -1;
    this.state = {
      value: this.props.multiple ? this.props.value ? this.props.value?.value : [default_value] : this.props.value ? this.getElementByValue(this.props.value) : default_value
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data != this.props.data) {
      this.setState({
        data: [...nextProps.data]
      });
    }

    if (nextProps.value != this.state.value) {
      this.setState({
        value: nextProps.value
      });
    }
  }

  isEnabled() {
    const {
      field,
      title,
      data,
      multiple,
      values,
      placeholder
    } = this.props;
    const {
      value
    } = this.state;
    const d = data && data.length > 0 ? this.props.data.map((e, i) => {
      const label = Array.isArray(this.props.visibled_value) ? this.props.visibled_value.map(visibled_value => e[visibled_value]).join(" ") : e[this.props.visibled_value];
      return {
        value: e[getIdName()],
        label,
        color: e[this.props.color]
      };
    }) : values ? values.map((e, i) => {
      const label = e.title;
      return {
        value: e._id,
        label,
        color: e[this.props.color]
      };
    }) : []; // console.log(field, data, d, this.props.visibled_value);

    const options = multiple ? (value || []).map((e, i) => {
      const label = Array.isArray(this.props.visibled_value) ? this.props.visibled_value.map(visibled_value => e[visibled_value]).join(" ") : e[this.props.visibled_value];
      const dat = {
        value: e[getIdName()],
        label,
        color: e.color
      };
      return dat;
    }) : value?.value ? value : {
      value: value,
      id: value,
      color: "#000000"
    };
    const colourStyles = {
      control: styles => ({ ...styles,
        backgroundColor: "#EFEFEF",
        color: data.color ? data.color : "#333"
      }),
      option: (styles, {
        data,
        isDisabled,
        isFocused,
        isSelected
      }) => {
        const clr = data.color;
        const color = chroma(clr || "#111");
        return { ...styles,
          backgroundColor: isDisabled ? null : isSelected ? clr : isFocused ? color.alpha(0.1).css() : null,
          color: isDisabled ? "#ccc" : isSelected ? chroma.contrast(color, "white") > 2 ? "white" : "black" : clr,
          cursor: isDisabled ? "not-allowed" : "default",
          ":active": { ...styles[":active"],
            backgroundColor: !isDisabled && (isSelected ? clr : color.alpha(0.3).css())
          }
        };
      },
      multiValue: (styles, {
        data
      }) => {
        const clr = data.color ? data.color : "#333";
        const color = chroma(clr || "#111");
        return { ...styles,
          backgroundColor: color.alpha(0.1).css()
        };
      },
      multiValueLabel: (styles, {
        data
      }) => ({ ...styles,
        color: data.color ? data.color : "#111"
      }),
      multiValueRemove: (styles, {
        data
      }) => ({ ...styles,
        color: data.color ? data.color : "#333",
        ":hover": {
          backgroundColor: data.color ? data.color : "#333",
          color: "white"
        }
      })
    };
    return /*#__PURE__*/React.createElement(Suspense, {
      fallback: /*#__PURE__*/React.createElement(Loading, null)
    }, /*#__PURE__*/React.createElement(Select, {
      value: options,
      styles: colourStyles,
      isMulti: multiple,
      isSearchable: true,
      onChange: this.handleChange,
      options: d,
      placeholder: placeholder || __("Select..."),
      className: "basic-multi-select w-100",
      classNamePrefix: "select"
    }));
  }

  isDesabled() {
    const {
      field,
      title,
      data,
      multiple
    } = this.props;
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 mb-1"
    }, (Array.isArray(value) ? value : [value]).map((e, i) => {
      const txt = typeof e === "string" ? e : e[this.props.visibled_value] ? e[this.props.visibled_value] : __("Untitled");
      return /*#__PURE__*/React.createElement(Tag, {
        minimal: true,
        className: "m-1",
        key: i
      }, txt);
    }));
  }

}