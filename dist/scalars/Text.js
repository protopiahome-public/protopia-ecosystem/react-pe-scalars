function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import ScalarField from "./ScalarField"; //import TextEditor from "../../utilities/TextEditor"

import { TextEditor } from "react-pe-useful";
export default class Text extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onChange", data => {
      this.setState({
        value: data
      });
      this.on(data);
    });
  }

  isEnabled() {
    return this.textEditor();
  }

  isDesabled() {
    return this.props.value;
  }

  textEditor() {
    return /*#__PURE__*/React.createElement(TextEditor, {
      onChange: this.onChange,
      text: this.state.value
    });
    /* return <textarea onChange={this.onChange} className="form-control" rows="10" value={this.state.value} >
    {this.state.value}
    </textarea>;
    */
  }

}