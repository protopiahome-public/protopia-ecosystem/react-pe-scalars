function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { Button, Tag } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
export default class ImageRadio extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onClick", evt => {
      const value = evt.currentTarget.getAttribute("v");
      this.setState({
        value
      });
      this.on(value);
    });

    _defineProperty(this, "on", value => {
      // console.log(value, this.props.field, this.props.title);
      if (this.props.on) this.props.on(value, this.props.field, this.props.title);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value) this.setState({
      value: nextProps.value
    });
  }

  isEnabled() {
    if (Array.isArray(this.props.values)) {
      const curVal = this.props.values.filter(e => e._id === this.state.value)[0];
      return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
        className: "d-flex flex-wrap"
      }, this.props.values.map((e, i) => {
        if (e.hidden) return null;
        return /*#__PURE__*/React.createElement(Button, {
          key: i,
          className: "scalar-imgage-radio-label",
          onClick: this.onClick,
          v: e._id,
          minimal: e._id != this.state.value
        }, /*#__PURE__*/React.createElement("img", {
          src: e.img,
          className: "scalar-imgage-radio-img",
          style: {
            height: e.height,
            top: 0,
            opacity: e._id == this.state.value ? e.icon_opacity ? e.icon_opacity : 1 : 0.125
          }
        }), /*#__PURE__*/React.createElement("div", {
          className: "scalar-imgage-radio-title"
        }, e.title), /*#__PURE__*/React.createElement("div", {
          className: "scalar-imgage-radio-description"
        }, e.desription));
      })), /*#__PURE__*/React.createElement("div", {
        className: "scalar-commentary",
        dangerouslySetInnerHTML: {
          __html: __(curVal ? curVal.commentary : null)
        }
      }));
    }

    this.isDesabled();
  }

  isDesabled() {
    const {
      field,
      title
    } = this.props;
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null);
  }

}