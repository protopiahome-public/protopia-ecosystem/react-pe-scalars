function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Suspense } from "react";
import { Button } from "@blueprintjs/core";
import PhoneInput, { isValidPhoneNumber } from "react-phone-number-input";
import "react-responsive-ui/style.css";
import { __ } from "react-pe-utilities";
import { Loading } from "react-pe-useful"; // TODO extends ScalarField

export default class Phone extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      value: this.props.value
    });

    _defineProperty(this, "onChange", value => {
      this.setState({
        value
      });
      this.on(value);
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });
  }

  render() {
    const {
      field,
      title
    } = this.props;
    const {
      value
    } = this.state;
    const col1 = this.props.vertical ? "col-12 layout-label-vert" : "col-md-3  layout-label";
    const col2 = this.props.vertical ? "col-12 layout-data-vert" : "col-md-9 layout-data";
    return /*#__PURE__*/React.createElement("div", {
      className: "row dat",
      key: field
    }, /*#__PURE__*/React.createElement("div", {
      className: col1
    }, __(title)), /*#__PURE__*/React.createElement("div", {
      className: col2 + " align-items-start"
    }, this.props.editable ? /*#__PURE__*/React.createElement("div", {
      className: "mb-4 w-100"
    }, /*#__PURE__*/React.createElement(Suspense, {
      fallback: /*#__PURE__*/React.createElement(Loading, null)
    }, /*#__PURE__*/React.createElement(PhoneInput, {
      country: "RU",
      placeholder: __("Enter phone number"),
      value: this.state.value,
      onChange: this.onChange,
      error: value ? isValidPhoneNumber(value) ? undefined : __("Invalid phone number") : __("")
    }))) : /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value)));
  }

}