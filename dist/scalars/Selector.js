function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Suspense } from "react"; //import Select from "react-select" // https://github.com/JedWatson/react-select 

import chroma from "chroma-js";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
import { getIdName } from "react-pe-layouts";
import { Loading } from "react-pe-useful";
import Select from "react-select";
export default class Selector extends ScalarField {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleChange", newValue => {
      const nnn = {};
      nnn[getIdName()] = newValue.value;
      nnn[this.props.visibled_value] = newValue.label; // nnn.__typename	= newValue.label;
      // delete newValue.__typename;
      // console.log( newValue );

      this.on(nnn);
      this.setState({
        value: nnn
      });
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });

    const mock = {};
    mock[getIdName()] = -1;
    this.state = {
      value: this.props.value ? this.props.value : [mock]
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data != this.props.data) {
      this.setState({
        data: [...nextProps.data]
      });
    }

    if (nextProps.value != this.state.value) {
      this.setState({
        value: nextProps.value
      });
    }
  }

  isEnabled() {
    const {
      field,
      title,
      data,
      multiple
    } = this.props;
    const {
      value
    } = this.state; // console.log(typeof this.props.data[0][getIdName()]);

    const noneVal = this.props.data[0] && typeof this.props.data[0][getIdName()] == "string" ? "" : -1;
    let d = [{
      value: noneVal,
      label: "---"
    }];
    d = d.concat(this.props.data ? this.props.data.map((e, i) => {
      const label = e[this.props.visibled_value];
      return {
        value: e[getIdName()],
        label,
        color: e[this.props.color]
      };
    }) : null); // console.log(  d  );

    return /*#__PURE__*/React.createElement(Suspense, {
      fallback: /*#__PURE__*/React.createElement(Loading, null)
    }, /*#__PURE__*/React.createElement(Select, {
      value: {
        value: value ? value[getIdName()] : null,
        label: value ? value[this.props.visibled_value] : null
      },
      isMulti: false,
      isSearchable: true,
      onChange: this.handleChange,
      options: d,
      placeholder: __("Select..."),
      className: "basic-multi-select",
      classNamePrefix: "select"
    }));
  }

  isDesabled() {
    const {
      field,
      title,
      data,
      multiple
    } = this.props;
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 mb-1"
    }, value ? value[this.props.visibled_value] : null);
  }

}