function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import ScalarField from "./ScalarField";
import { MediaChooser } from "react-pe-useful";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { server_url__ } from "settings/config";
import { Button, Dialog } from '@blueprintjs/core'; //  Scalar  String

class Media extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onOpen", () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    _defineProperty(this, "onMediaChange", (value, file, id) => {
      // console.log(value);
      const state = {
        value
      }; // state[this.props.field + "_name"] =

      this.on(value, this.props.field, file.name, id);
      this.setState(state);
    });

    _defineProperty(this, "on", (value, name, fileName, id = -1) => {
      const anoverField = {};
      anoverField[`${this.props.field}_id`] = id;
      anoverField[`${this.props.field}_name`] = fileName; // console.log(  value, this.props.field, this.props.title, anoverField );

      this.props.on(value, this.props.field, this.props.title, anoverField);
    });
  }

  isEnabled() {
    const {
      field
    } = this.props;
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: "my-2 w-100"
    }, /*#__PURE__*/React.createElement(MediaChooser, {
      prefix: `_${field}${this.props.id}`,
      url: value,
      id: "",
      ID: "",
      padding: 5,
      height: 120,
      tab: this.tab,
      hideLib: this.props.hideLib,
      accept: this.props.accept,
      isUploadHide: this.props.isUploadHide,
      isURLHide: this.props.isURLHide,
      onChange: this.onMediaChange,
      server_url: server_url__()
    }));
  }

  isDesabled() {
    const {
      value
    } = this.state;
    const {
      width,
      height
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-1 media-input container"
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        backgroundImage: `url(${value})`,
        backgroundSize: "cover",
        width: width ? width : 160,
        height: height ? height : 160,
        opacity: 0.8,
        margin: 6,
        cursor: "pointer"
      },
      className: "media-input",
      onClick: this.onOpen
    }), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isOpen,
      onClose: this.onOpen
    }, /*#__PURE__*/React.createElement("div", {
      className: "flex-centered"
    }, /*#__PURE__*/React.createElement("img", {
      src: value,
      alt: "",
      style: {
        maxHeight: "90vh"
      }
    }))));
  }

}

export default compose(withApollo)(Media);