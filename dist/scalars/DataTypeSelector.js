function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Tag } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
import { getTypeSelector } from "react-pe-layouts";
export default class DataTypeSelector extends ScalarField {
  constructor(props) {
    super(props);

    _defineProperty(this, "onChange", evt => {
      this.setState({
        value: evt.currentTarget.value
      });
      this.on(evt.currentTarget.value);
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });

    this.state = {
      value: this.props.value
    };
  }

  isDesabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: `datetimer ${this.props.className}`
    }, /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null));
  }

  isEnabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: `datetimer w-100 ${this.props.className}`
    }, getTypeSelector({
      onChange: this.onChange,
      selected: this.state.value
    }));
  }

}