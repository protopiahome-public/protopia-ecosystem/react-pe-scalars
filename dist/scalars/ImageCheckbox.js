function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { Button, Tag } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
export default class ImageCheckbox extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      value: Array.isArray(this.props.value) ? this.props.value : [this.props.value]
    });

    _defineProperty(this, "onClick", (evt, i) => {
      const value = evt.currentTarget.getAttribute("v");
      const state = { ...this.state
      };
      state.value[i] = state.value[i] ? false : value;
      this.setState(state);
      this.on(state.value);
    });

    _defineProperty(this, "on", value => {
      // console.log(value, this.props.field, this.props.title);
      if (this.props.on) this.props.on(value, this.props.field, this.props.title);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value) this.setState({
      value: nextProps.value
    });
  }

  isEnabled() {
    if (Array.isArray(this.props.values)) {
      return /*#__PURE__*/React.createElement("div", {
        className: "d-flex flex-wrap"
      }, this.props.values.map((e, i) => {
        const checked = Array.isArray(this.state.value) ? this.state.value.filter(v => v === e._id).length > 0 : false;
        return /*#__PURE__*/React.createElement(Button, {
          key: i,
          className: "scalar-imgage-radio-label",
          onClick: evt => this.onClick(evt, i),
          v: e._id,
          minimal: !checked
        }, /*#__PURE__*/React.createElement("img", {
          src: e.img,
          className: "scalar-imgage-radio-img",
          style: {
            height: e.height,
            top: 0,
            opacity: checked ? e.icon_opacity ? e.icon_opacity : 1 : 0.125
          }
        }), /*#__PURE__*/React.createElement("div", {
          className: "scalar-imgage-radio-title"
        }, e.title), /*#__PURE__*/React.createElement("div", {
          className: "scalar-imgage-radio-description"
        }, e.desription));
      }));
    } // this.isDesabled()

  }

  isDesabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null);
  }

}