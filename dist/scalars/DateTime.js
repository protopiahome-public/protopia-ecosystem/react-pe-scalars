function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { Card, Classes, FormGroup, Button, Tooltip, Position, Popover, ButtonGroup } from "@blueprintjs/core";
import { DateInput, IDateFormatProps, TimePrecision, DateTimePicker } from "@blueprintjs/datetime";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
import Moment from "react-moment";
import MomentLocaleUtils, { formatDate, parseDate } from "react-day-picker/moment";
import "react-day-picker/lib/style.css";
import DayPicker from "react-day-picker";
import moment from "moment";
import "moment/locale/ru";
export default class DateTime extends ScalarField {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleStartChange", value => {
      this.setState({
        value
      });
      const state = { ...this.state,
        value
      }; // this.on(Date.parse(value)/1000);
      //console.log(value.getTime())

      this.on(value.getTime()); //this.on(moment(value).toISOString())
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });

    const now = Date.now();
    const d = new Date(now);
    const plusm = d.setMonth(d.getMonth() + 2);
    let {
      range
    } = props;
    range = range == [] ? [new Date(range[0]), new Date(range[1])] : [new Date(now), new Date(plusm)]; // console.log(props);
    // console.log(now);

    this.state = {
      value: new Date(this.props.value || now),
      range,
      date: new Date(this.props.value || now)
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value) this.setState({
      value: new Date(nextProps.value),
      date: new Date(nextProps.value)
    });
  }

  render() {
    const {
      field,
      title
    } = this.props;
    const jsDateFormatter = {
      // note that the native implementation of Date functions differs between browsers
      formatDate: date => moment(date).format("D.MM.YYYY HH:mm"),
      // parseDate: str => new Date(str),
      parseDate: str => new Date(Date.parse(str)),
      placeholder: "M/D/YYYY"
    };
    const col1 = this.props.vertical ? "col-12 layout-label-vert" : "col-md-3  layout-label";
    const col2 = this.props.vertical ? "col-12 layout-data-vert" : "col-md-9 layout-data";
    return /*#__PURE__*/React.createElement("div", {
      className: "row  dat",
      key: field
    }, /*#__PURE__*/React.createElement("div", {
      className: col1
    }, __(title)), /*#__PURE__*/React.createElement("div", {
      className: col2
    }, this.props.editable ? /*#__PURE__*/React.createElement("div", {
      className: `datetimer w-100 mb-2 ${this.props.className}`
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-calendar-alt pr-1 fa-15x"
    }), /*#__PURE__*/React.createElement(DateInput, _extends({
      minDate: new Date(new Date().setFullYear(new Date().getFullYear() - 100)),
      maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 10))
    }, jsDateFormatter, {
      className: " " + "",
      closeOnSelection: true,
      value: new Date(this.state.value),
      date: this.state.date,
      defaultValue: new Date(this.state.date),
      onChange: this.handleStartChange,
      invalidDateMessage: __("Invalid date"),
      timePrecision: TimePrecision.MINUTE,
      timePickerProps: {
        showArrowButtons: true
      }
    }))) : /*#__PURE__*/React.createElement("div", {
      className: "d-flex my-1"
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-calendar-alt pr-1"
    }), /*#__PURE__*/React.createElement("div", {
      className: ""
    }, /*#__PURE__*/React.createElement(Moment, {
      locale: "ru",
      format: "D MMMM YYYY HH:mm"
    }, new Date(this.state.value))))));
  }

  isEnabled() {
    const {
      field,
      title,
      value,
      className
    } = this.props;
    const jsDateFormatter = {
      // note that the native implementation of Date functions differs between browsers
      formatDate: date => moment(date).format("D.MM.YYYY HH:mm"),
      // parseDate: str => new Date(str),
      parseDate: str => new Date(Date.parse(str)),
      placeholder: __("Time")
    };
    return /*#__PURE__*/React.createElement("div", {
      className: `datetimer ${this.props.className}`
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-calendar-alt pr-1 fa-15x"
    }), /*#__PURE__*/React.createElement(DateInput, _extends({
      minDate: new Date(new Date().setFullYear(new Date().getFullYear() - 100)),
      maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 10))
    }, jsDateFormatter, {
      invalidDateMessage: __("Invalid date"),
      timePrecision: TimePrecision.MINUTE,
      className: " " + "",
      closeOnSelection: true,
      date: this.state.date,
      defaultValue: new Date(),
      onChange: this.handleStartChange
    })));
  }

  isDesabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: "d-flex my-1"
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-calendar-alt pr-1"
    }), /*#__PURE__*/React.createElement("div", {
      className: ""
    }, /*#__PURE__*/React.createElement(Moment, {
      locale: "ru",
      format: "D MMMM YYYY"
    }, new Date(this.state.value))));
  }

  dtp() {
    return /*#__PURE__*/React.createElement(DateTimePicker, {
      className: Classes.ELEVATION_1,
      value: this.state.date,
      timePickerProps: {
        precision: "second",
        useAmPm: true
      },
      onChange: this.handleStartChange
    });
  }

}