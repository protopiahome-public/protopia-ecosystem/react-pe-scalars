function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { Component } from "react";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
import TagScalars from "./tags/TagScalars"; //  Scalar  Int

export default class Tags extends ScalarField {
  isEnabled() {
    const {
      field,
      title
    } = this.props;
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(TagScalars, _extends({}, this.props, {
      value: value,
      values: this.props.values,
      onChange: this.props.onChange
    })));
  }

}