function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Tag, Button, Popover, Position } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import SubMenu from "./SubMenu"; // TODO rename to ScalarField

export default class ScalarField extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onChange", evt => {
      this.setState({
        value: evt.currentTarget.value
      });
      this.on(evt.currentTarget.value);
    });

    _defineProperty(this, "onClear", evt => {
      this.setState({
        value: ""
      });
      this.on("");
    });

    _defineProperty(this, "on", val => {
      if (!this.props.on) return;
      const value = this.props.prefix ? val.replace(this.props.prefix, "") : val;
      this.props.on(value, this.props.field, this.props.title);
    });

    _defineProperty(this, "onSubMenu", (val, field, title, prefix) => {
      if (!this.props.onSubMenu) return; //const value = prefix ? val.replace(prefix, "") : val
      //console.log(val, field)

      this.props.onSubMenu(val, field);
    });

    this.state = {
      value: this.props.value || props.default
    };
    window[`form${this.props._id}`] = this;
  }

  componentWillReceiveProps(nextProps) {
    if (typeof nextProps.value !== "undefined" && this.state.value !== nextProps.value) this.setState({
      value: nextProps.value
    });
  }

  render() {
    // console.log(this.props.field);
    const {
      title,
      description,
      commentary,
      sub_menus
    } = this.props;
    const col1 = title || description ? this.props.vertical ? "col-12 layout-label-vert " : "col-md-3  layout-label" : " hidden ";
    const col2 = title || description ? this.props.vertical ? "col-12 layout-data-vert " : "col-md-9 layout-data" : " col-12 layout-data-vert ";
    const descr = description ? /*#__PURE__*/React.createElement(Popover, {
      position: Position.RIGHT,
      interactionKind: "hover",
      content: /*#__PURE__*/React.createElement("div", {
        className: "p-4 square",
        dangerouslySetInnerHTML: {
          __html: __(description)
        }
      })
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      icon: "help"
    })) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: "row dat strob01 ",
      row_data: this.props.field
    }, /*#__PURE__*/React.createElement("div", {
      className: col1
    }, __(title), descr), /*#__PURE__*/React.createElement("div", {
      className: col2
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100"
    }, this.props.prefix ? /*#__PURE__*/React.createElement("span", {
      className: "prefix"
    }, this.props.prefix) : null, /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-grow-100 max-width-100"
    }, this.props.editable ? this.isEnabled() : this.isDesabled()), this.props.postfix ? /*#__PURE__*/React.createElement("span", {
      className: "postfix"
    }, this.props.postfix) : null, this.props.editable && !this.props.not_clear ? /*#__PURE__*/React.createElement(Button, {
      className: "right",
      icon: "cross",
      minimal: true,
      onClick: this.onClear
    }) : null), /*#__PURE__*/React.createElement(SubMenu, {
      sub_menus: sub_menus,
      on: this.onSubMenu
    })), commentary ? /*#__PURE__*/React.createElement("div", {
      className: "scalar-commentary px-3 col-md-9 offset-md-3",
      dangerouslySetInnerHTML: {
        __html: __(commentary)
      }
    }) : null);
  }

  isEnabled() {
    let {
      value
    } = this.state;

    if (this.props.prefix && typeof this.state.value == "string") {
      value = this.state.value.replace(this.props.prefix, "");
    }

    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("input", {
      autoFocus: this.props.autoFocus,
      type: this.props.type,
      className: `${this.props.className ? this.props.className : "form-control input dark"} flex-grow-100 pr-5`,
      value: value || "",
      onChange: this.onChange
    }));
  }

  isDesabled() {
    const {
      field
    } = this.props;
    const visibled_value = this.props.visibled_value || "title";
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 mb-1"
    }, this.props.value ? visibled_value === field ? /*#__PURE__*/React.createElement("div", {
      className: "lead"
    }, `${this.props.value} `) : /*#__PURE__*/React.createElement(Tag, {
      minimal: true,
      className: `m-1 ${this.props.className}`
    }, `${this.props.value} `) : null);
  }

}