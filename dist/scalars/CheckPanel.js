function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Button, Tag } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField"; // TODO extends ScalarField

export default class CheckPanel extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onClick", (evt, i) => {
      const value = [...this.props.value];
      value[i] = !evt.currentTarget.classList.contains('active');
      this.on(value);
    });
  }

  isEnabled() {
    const {
      values
    } = this.props;
    const value = Array.isArray(this.props.value) ? value : [value];
    return Array.isArray(values) ? /*#__PURE__*/React.createElement("div", {
      class: "pe-cp-buttons"
    }, values.map((e, i) => {
      const d = e._id ? e : {
        _id: e,
        title: e
      };
      const is = value.filter(v => v === d._id).length > 0;
      return /*#__PURE__*/React.createElement("div", {
        class: `pe-cp-button ${is ? "active" : ""}`,
        key: i,
        onClick: evt => this.onClick(evt, i)
      }, d.title);
    })) : null;
  }

  isDesabled() {
    const {
      value
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-1 "
    }, /*#__PURE__*/React.createElement(Tag, {
      intent: Intent.DANGER
    }, value.join(" ")));
  }

}