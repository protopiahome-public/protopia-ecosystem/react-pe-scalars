function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Tag } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import { default as UUID } from "node-uuid"; //  Scalar  ID
// TODO extends ScalarField

export default class ID extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onChange", evt => {
      this.setState({
        value: evt.currentTarget.value
      });
      this.on(evt.currentTarget.value);
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });

    this.state = {
      value: typeof this.props.value === "undefined" ? UUID.v4() : this.props.value
    };
  }

  isEnabled() {
    const {
      field,
      title,
      value,
      extended_link,
      external_link_data
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: `datetimer ${this.props.className}`
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-fingerprint"
    }), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "",
      value: value || "",
      onChange: this.onChange
    }));
  }

  isDesabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: `datetimer ${this.props.className}`
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-fingerprint"
    }), /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null));
  }

  render() {
    const {
      field,
      title
    } = this.props;
    const {
      value
    } = this.state;
    const col1 = this.props.vertical ? "col-12 layout-label-vert" : "col-md-3  layout-label";
    const col2 = this.props.vertical ? "col-12 layout-data-vert" : "col-md-7 layout-data";
    return /*#__PURE__*/React.createElement("div", {
      className: "row  dat",
      key: field
    }, /*#__PURE__*/React.createElement("div", {
      className: col1
    }, __(title)), /*#__PURE__*/React.createElement("div", {
      className: col2
    }, /*#__PURE__*/React.createElement("div", {
      className: `datetimer ${this.props.className}`
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-fingerprint"
    }), this.props.editable ? /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "",
      value: value || "",
      onChange: this.onChange
    }) : /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null))));
  }

}