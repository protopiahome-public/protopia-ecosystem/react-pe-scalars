function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import SubMenu from "./SubMenu"; // TODO extends ScalarField

export default class Radio extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      value: this.props.value,
      data: [{
        _id: 11,
        title: "AAAA"
      }, {
        _id: 12,
        title: "BBBB"
      }, {
        _id: 13,
        title: "CCCC"
      }, {
        _id: 14,
        title: "DDDD"
      }, {
        _id: 15,
        title: "EEEE"
      }]
    });

    _defineProperty(this, "getComponent", () => {
      const {
        component,
        values
      } = this.props;
      const {
        value
      } = this.state; // console.log( value );

      let _component;

      if (typeof component === "String") {
        _component = this.state.data;
      } else if (component === "PlaceType") {
        const items = Array.isArray(values) ? values : Array.isArray(component) ? component : null; // console.log(items);

        _component = items ? items.map((e, i) => {
          const elem = typeof e._id !== "undefined" ? e : {
            _id: e._id,
            title: e.title,
            disabled: e.disabled,
            description: ""
          }; //const id = `__${this.props._id}_${elem._id}`

          return /*#__PURE__*/React.createElement("div", {
            className: "pb-0 mb-1",
            key: i
          }, /*#__PURE__*/React.createElement("label", {
            className: ` _check_blue_ ${elem.disabled ? "disabled" : ""}`
          }, /*#__PURE__*/React.createElement("input", {
            value: elem._id,
            type: "radio",
            checked: elem._id === value,
            disabled: elem.disabled,
            onChange: this.onChange
          }), __(elem.title)), /*#__PURE__*/React.createElement("div", null, __(elem.description)), elem.commentary ? /*#__PURE__*/React.createElement("div", {
            className: "small text-italic mb-3 opacity_75"
          }, __(elem.commentary)) : null);
        }) : null;
      } else {
        const items = Array.isArray(values) ? values : Array.isArray(component) ? component : [values]; // console.log(items, values, value);

        _component = items ? items.map((e, i) => {
          const elem = typeof e._id != "undefined" ? e : {
            _id: e,
            title: __(e),
            disabled: false,
            description: ""
          };
          const id = `__${this.props._id}_${elem._id}`; // console.log(elem._id, value, elem._id == value);

          return /*#__PURE__*/React.createElement("div", {
            className: "pb-0 mb-1",
            key: i
          }, /*#__PURE__*/React.createElement("label", {
            className: `${elem.marked ? '_check_red_' : '_check_blue_'} ${elem.disabled ? "disabled" : ""}`,
            htmlFor: id
          }, /*#__PURE__*/React.createElement("input", {
            value: elem._id,
            type: "radio",
            checked: elem._id === value,
            onChange: this.onChange,
            onClick: this.onChange,
            disabled: elem.disabled,
            id: id
          }), __(elem.title)), elem.commentary ? /*#__PURE__*/React.createElement("div", {
            className: "small text-italic mb-3 opacity_75"
          }, __(elem.commentary)) : null);
        }) : null;
      }

      return _component;
    });

    _defineProperty(this, "onChange", evt => {
      const {
        value
      } = evt.currentTarget;
      this.setState({
        value
      });
      this.on(value);
    });

    _defineProperty(this, "on", value => {
      // console.log(value, this.props.field, this.props.title);
      this.props.on(value, this.props.field, this.props.title);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value) this.setState({
      value: nextProps.value
    });
  }

  render() {
    const {
      field,
      title,
      commentary,
      sub_menus
    } = this.props;
    const col1 = this.props.vertical ? "col-12 layout-label-vert" : "col-md-3  layout-label";
    const col2 = this.props.vertical ? "col-12 layout-data-vert" : "col-md-9 layout-data";
    return /*#__PURE__*/React.createElement("div", {
      className: "row dat",
      key: field
    }, /*#__PURE__*/React.createElement("div", {
      className: col1
    }, __(title)), /*#__PURE__*/React.createElement("div", {
      className: col2
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100"
    }, this.props.prefix ? /*#__PURE__*/React.createElement("span", {
      className: "prefix"
    }, this.props.prefix) : null, /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-grow-100"
    }, this.props.editable ? /*#__PURE__*/React.createElement("div", {
      className: "my-2"
    }, this.getComponent()) : /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-1"
    }, this.props.value)), this.props.postfix ? /*#__PURE__*/React.createElement("span", {
      className: "postfix"
    }, this.props.postfix) : null, this.props.editable && !this.props.not_clear ? /*#__PURE__*/React.createElement(Button, {
      className: "right",
      icon: "cross",
      minimal: true,
      onClick: this.onClear
    }) : null), /*#__PURE__*/React.createElement(SubMenu, {
      sub_menus: sub_menus,
      on: this.onSubMenu
    })), commentary ? /*#__PURE__*/React.createElement("div", {
      className: "scalar-commentary px-3 col-md-9 offset-md-3",
      dangerouslySetInnerHTML: {
        __html: __(commentary)
      }
    }) : null);
  }

}