function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Tag, ButtonGroup, Button, Intent } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
export default class Reloadbled extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onLoad", () => {
      this.setState({
        btn_class: " fa-spin"
      });
    });
  }

  isEnabled() {
    const {
      field,
      title
    } = this.props;
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: "input-group mb-3"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: this.props.className ? this.props.className : "form-control input dark",
      style: {
        fontSize: "0.8rem"
      },
      value: value || "",
      onChange: this.onChange,
      disabled: true
    }), /*#__PURE__*/React.createElement("div", {
      className: "input-group-append"
    }, /*#__PURE__*/React.createElement("div", {
      className: "btn btn-secondary",
      onClick: this.onLoad
    }, /*#__PURE__*/React.createElement("i", {
      className: `fas fa-sync-alt ${this.state.btn_class}`
    }))));
  }

}