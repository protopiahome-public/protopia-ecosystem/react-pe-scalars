function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Suspense, lazy } from "react";
import { Tag, Button, Intent, Tabs, Tab } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
import { Loading, FontAwesome } from "react-pe-useful";
import { MediaChooser } from "react-pe-useful"; // const FontAwesome = lazy(
// 	() => import("react-pe-useful")
// 		.then(module => ({ default: module.FontAwesome }))
// )

export default class Icon extends ScalarField {
  constructor(props) {
    super(props);

    _defineProperty(this, "onNavBar", navBarID => this.setState({
      navBarID
    }));

    _defineProperty(this, "onValue", value => {
      this.setState({
        value
      });
      this.onChangeIcon();
    });

    _defineProperty(this, "onSize", size => {
      this.setState({
        size: " fa-" + size + "x"
      });
      this.onChangeIcon();
    });

    _defineProperty(this, "onChangeIcon", () => {
      //console.log(this.state.value, this.state.size)
      setTimeout(() => this.on(this.state.value + this.state.size), 200);
    });

    _defineProperty(this, "onImage", src => {
      setTimeout(() => this.on(src), 200);
    });

    if (!this.state) this.state = {};
    this.state = { ...this.state,
      ...this.extractValue(props.value)
    };
    this.state.navBarID = this.state.isSVG ? "image" : "icon";
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.value !== nextProps.value) {
      const values = this.extractValue(nextProps.value); //console.log( values )

      this.setState({
        value: values.value,
        size: values.size,
        navBarID: values.isSVG ? "image" : "icon"
      });
    }
  }

  extractValue(val) {
    if (typeof val !== "string") return {
      value: "",
      size: "",
      isSVG: true
    };
    let isSVG = val ? val.split(".").length > 1 : false,
        value = val,
        size = "";

    if (!isSVG) {
      value = val.replaceAll("fa-1x", "").replaceAll("fa-2x", "").replaceAll("fa-3x", "");
      size = val.replaceAll(value, "");
      size = size.indexOf("fa-1x") ? " fa-1x" : size.indexOf("fa-2x") ? " fa-2x" : size.indexOf("fa-3x") ? " fa-3x" : size;
    }

    return {
      value,
      size,
      isSVG
    };
  }

  isEnabled() {
    const {
      isSVG,
      value
    } = this.state; //return isSVG ? this.isIcon(): this.isImage()

    return /*#__PURE__*/React.createElement("div", {
      style: {
        height: 220,
        width: "100%"
      }
    }, /*#__PURE__*/React.createElement(Tabs, {
      animate: false,
      selectedTabId: this.state.navBarID,
      onChange: this.onNavBar,
      vertical: true,
      className: "w-100 icon-scalar-container"
    }, /*#__PURE__*/React.createElement(Tab, {
      id: "image",
      title: /*#__PURE__*/React.createElement("div", {
        className: "d-flex flex-column align-items-center"
      }, /*#__PURE__*/React.createElement("i", {
        className: "far fa-image "
      }), /*#__PURE__*/React.createElement("div", {
        className: "super-small"
      }, __("image"))),
      className: "w-100",
      panel: /*#__PURE__*/React.createElement(MediaChooser, {
        url: value,
        onChange: this.onImage,
        id: "",
        isUploadHide: this.props.isUploadHide,
        isURLHide: this.props.isURLHide
      })
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "icon",
      title: /*#__PURE__*/React.createElement("div", {
        className: "d-flex flex-column align-items-center"
      }, /*#__PURE__*/React.createElement("i", {
        className: "fas fa-asterisk "
      }), /*#__PURE__*/React.createElement("div", {
        className: "super-small"
      }, __("icon"))),
      className: "w-100",
      panel: this.getIcon()
    })));
  }

  getIcon() {
    const {
      value,
      size
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: `d-flex w-100 ${this.props.className}`,
      style: {
        height: 199
      }
    }, /*#__PURE__*/React.createElement(Suspense, {
      fallback: /*#__PURE__*/React.createElement(Loading, null)
    }, /*#__PURE__*/React.createElement(FontAwesome, {
      value: value,
      onChange: this.onValue
    })), /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      intent: size == "fa-1x" ? Intent.DANGER : Intent.NONE,
      large: true,
      className: "ml-auto font-weight-bold ",
      onClick: () => this.onSize(1)
    }, "1x"), /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      intent: size == "fa-2x" ? Intent.DANGER : Intent.NONE,
      large: true,
      className: " font-weight-bold ",
      onClick: () => this.onSize(2)
    }, "2x"), /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      intent: size == "fa-3x" ? Intent.DANGER : Intent.NONE,
      large: true,
      className: " font-weight-bold ",
      onClick: () => this.onSize(3)
    }, "3x"));
  }

  isDesabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: ` ${this.props.className}`
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-at"
    }), /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null));
  }

}