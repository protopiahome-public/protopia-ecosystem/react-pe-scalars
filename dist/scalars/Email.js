function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Tag, Button } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
export default class Email extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onChange", evt => {
      this.setState({
        value: evt.currentTarget.value
      });
      this.on(evt.currentTarget.value);
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });
  }

  isEnabled() {
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: `d-flex w-100 align-items-center ${this.props.className}`
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-at "
    }), /*#__PURE__*/React.createElement("div", {
      className: " w-100 datetimer"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "",
      value: value || "",
      onChange: this.onChange
    })));
  }

  isDesabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: `datetimer ${this.props.className}`
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-at"
    }), /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null));
  }

}