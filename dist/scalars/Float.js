import React, { Component } from "react";
import { Tag, ButtonGroup, Button, Intent } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField"; //  Scalar  Float

export default class Float extends ScalarField {}