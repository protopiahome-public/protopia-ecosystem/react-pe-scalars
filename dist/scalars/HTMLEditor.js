function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Button } from "@blueprintjs/core";
import ScalarField from "./ScalarField";
import { CodeEditor } from "react-pe-useful";
export default class HTMLEditor extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onChange", code => {
      this.setState({
        value: code
      });
      this.on(code);
    });
  }

  isEnabled() {
    const {
      value
    } = this.props;
    const val = value ? value : "";
    return /*#__PURE__*/React.createElement("div", {
      className: `d-flex w-100 align-items-start ${this.props.className ? this.props.className : ""}`
    }, /*#__PURE__*/React.createElement("div", {
      className: " pe-code-editor "
    }, /*#__PURE__*/React.createElement(CodeEditor, {
      value: val,
      onChange: this.onChange
    })));
  }

}