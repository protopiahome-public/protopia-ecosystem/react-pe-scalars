function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Tag, ButtonGroup, Button, Intent } from "@blueprintjs/core";
import rgbHex from "rgb-hex";
import { __ } from "react-pe-utilities";
import { ColorPicker } from "react-pe-useful";
import ScalarField from "./ScalarField";
export default class Color extends ScalarField {
  constructor(props) {
    super(props);

    _defineProperty(this, "distinations", () => {
      return ["up", "right", "down", "left"];
    });

    _defineProperty(this, "plainColor", () => {
      const {
        value
      } = this.state; // console.log( value )

      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ColorPicker, {
        color: this.getSingleColor(),
        onChoose: this.onColor
      }), /*#__PURE__*/React.createElement("input", {
        type: "string",
        className: "form-control input dark",
        value: value === null ? "" : value,
        onChange: this.onChange
      }));
    });

    _defineProperty(this, "gradient", () => {
      const colors = this.getGradientData();
      return /*#__PURE__*/React.createElement("div", {
        className: "w-100 d-flex"
      }, /*#__PURE__*/React.createElement(ColorPicker, {
        color: colors[0],
        onChoose: color => this.onColorGradient(color, 0)
      }), /*#__PURE__*/React.createElement(ColorPicker, {
        color: colors[1],
        onChoose: color => this.onColorGradient(color, 1)
      }), /*#__PURE__*/React.createElement(Button, {
        minimal: true,
        icon: "arrow-" + this.state.gradientDestination,
        style: {
          minWidth: 45
        },
        onClick: this.onDestination
      }), /*#__PURE__*/React.createElement("input", {
        disabled: true,
        className: "form-control input dark"
      }));
    });

    _defineProperty(this, "onClear", evt => {
      this.setState({
        value: null,
        gradientDestination: "up",
        current: "plainColor"
      });
      this.on(null);
    });

    _defineProperty(this, "onColor", color => {
      const c = color.rgb.a == 1.0 ? color.hex : color.rgb.a == 0.0 ? "transparent" : `#${rgbHex(color.rgb.r, color.rgb.g, color.rgb.b, color.rgb.a)}`;
      this.setState({
        value: c
      });
      this.on(c);
    });

    _defineProperty(this, "onColorGradient", (color, num) => {
      const c = color.rgb.a == 1.0 ? color.hex : color.rgb.a == 0.0 ? "transparent" : `#${rgbHex(color.rgb.r, color.rgb.g, color.rgb.b, color.rgb.a)}`;
      const colors = this.getGradientData();
      colors[num] = c;
      colors.push(this.state.gradientDestination);
      const newColors = colors.join("==color==");
      this.setState({
        color: newColors
      });
      this.on(newColors);
    });

    _defineProperty(this, "onDestination", () => {
      let dest = this.state.gradientDestination;
      this.distinations().forEach((e, i) => {
        if (e == this.state.gradientDestination) {
          dest = this.distinations()[(i + 1) % this.distinations().length];
        }
      });
      let colors = this.getGradientData();

      if (colors.length > 1) {
        colors[colors.length == 2 ? colors.length : colors.length - 1] = dest;
        this.on(colors.join("==color=="));
      }

      this.setState({
        gradientDestination: dest
      });
    });

    _defineProperty(this, "getGradientData", () => {
      let colors = this.state.value ? this.state.value.split("==color==") : [];
      colors.pop();
      return colors;
    });

    _defineProperty(this, "getGradientDestination", () => {
      let colors = this.state.value ? this.state.value.split("==color==") : [];
      let type = colors.pop();
      return colors.length > 1 ? type : "up";
    });

    _defineProperty(this, "switchers", () => {
      return this.state.mode == "all" ? /*#__PURE__*/React.createElement(ButtonGroup, {
        minimal: true
      }, /*#__PURE__*/React.createElement(Button, {
        icon: "stop",
        intent: this.state.current == "plainColor" ? Intent.DANGER : Intent.NONE,
        className: "hint hint--top d-flex",
        "data-hint": __("plain color"),
        onClick: () => this.setState({
          current: "plainColor"
        })
      }), /*#__PURE__*/React.createElement(Button, {
        icon: "list",
        intent: this.state.current == "gradient" ? Intent.DANGER : Intent.NONE,
        className: "hint hint--top d-flex",
        "data-hint": __("gradient"),
        onClick: () => this.setState({
          current: "gradient"
        })
      })) : null;
    });

    _defineProperty(this, "onChange", evt => {
      this.setState({
        value: evt.currentTarget.value
      });
      this.on(evt.currentTarget.value);
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });

    let current = "plainColor";

    if (props.value && this.getGradientData().length > 1) {
      current = "gradient";
    }

    this.state = {
      value: props.value,
      mode: props.mode ? props.mode : "color",
      //gradient, all
      current,
      gradientDestination: this.getGradientDestination()
    }; //console.log(current)
  }

  isDesabled() {
    const {
      field,
      title,
      value,
      extended_link,
      external_link_data
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: `datetimer ${this.props.className}`
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        width: 100,
        height: 25,
        backgroundColor: this.state.value
      }
    }));
  }

  isEnabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: `color w-100 d-flex ${this.props.className}`
    }, this[this.state.current](), this.switchers());
  }

  getSingleColor() {
    let colors = this.state.value ? this.state.value.split("==color==") : []; //console.log(colors.length)

    if (colors.length < 2) return this.state.value;
  }

}