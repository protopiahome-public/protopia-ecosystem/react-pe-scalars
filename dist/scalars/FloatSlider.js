function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { Tag, ButtonGroup, Button, Intent, TextArea, Slider } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
export default class FloatSlider extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onClear", evt => {
      this.setState({
        value: null
      });
      this.on(null);
    });

    _defineProperty(this, "onChangeSlider", value => {
      this.setState({
        value
      });
      this.on(value);
    });

    _defineProperty(this, "onChange", evt => {
      this.setState({
        value: evt.currentTarget.value
      });
      this.on(evt.currentTarget.value);
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });
  }

  isEnabled() {
    const {
      field,
      title
    } = this.props;
    const {
      value
    } = this.state; // console.log(value);

    return /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100"
    }, /*#__PURE__*/React.createElement(Slider, {
      min: this.props.min ? this.props.min : 0,
      max: this.props.max ? this.props.max : 100,
      stepSize: this.props.step_size ? this.props.step_size : 10,
      labelStepSize: this.props.label_step_size ? this.props.label_step_size : this.props.step_size ? this.props.step_size : 10,
      value: parseFloat(value) || (this.props.min > 0 ? this.props.min : 0),
      onChange: this.onChangeSlider,
      className: "my-2"
    }), /*#__PURE__*/React.createElement(Button, {
      className: "right",
      icon: "cross",
      minimal: true,
      onClick: this.onClear
    }));
  }

  isDesabled() {
    const {
      field,
      title
    } = this.props;
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null);
  }

}